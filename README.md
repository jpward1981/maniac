Maniac Framework
=========

# Purpose

This repository is part of a larger framework for managing your infrastructure-as-code in a manner that is consistent with the [Ansible Best Practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html) documentation. In addition to these best practices, this framework expands upon those best practices using some opinionated methods for solving problems not directly addressed in the best practices guidance and/or not automated with other Ansible tooling.

Planned and current features of the framework include:

    - [X] Automated dependent role detection and installation (added in v0.1.0)
    - [ ] Automated variable and inventory file management and installation
    - [ ] Automated role testing management and bootstrapping
    - [ ] Automated target environment conditional fact detection and import based on distro
    - [ ] System package installation function
    - [ ] System package version control function
    - [ ] System package selective upgrade function
    - [ ] System package full system upgrade function
    - [ ] Repository file and key management function

Note: This documentation will be significantly expanded and better structured in future releases.

# Requirements

- Ansible 2.8
- git

# Framework Components

The maniac framework is comprised of several components. The parts contained within this repository follow.

## Playbook

This repository contains a single `playbook.yml` file in the top-level directory that acts as the entrypoint for the framework. This playbook is the same, regardless of the underlying role(s) being called, and is designed to be as generic as possible. The Ansible Best Practices documentation currently recommends using a single playbook entrypoint to call other logic, like other playbooks; therefore, Dreamer Labs has taken the liberty of standardizing on a single, generic one. Removing the decision point of when and how to write the playbook for a role simplifies the process of using (and writing) a new role, and declutters ansible directories that centralize the management of multiple types of services.

## Documentation

Whenever possible, documentation is verbose enough to throughly explain both how things work, as well as _why_ the work that way. This documentation strive a good balance between maintaining a centralized entrypoint for docs, while also maintaining a distributed architecture that puts the docs in-line or close-to the things they are documenting.

## Engine Role

The `engine` role located in `roles/engine__<version>/` is the heart and soul of the maniac framework. It contains most of the logic/tasks within the framework. This role, like others, contains all its tasks files in its `tasks/` directory. Within this directory, there are a series of `.yml` files that act as entrypoints known as "stages" which call one or more backend "functions".

### Stages

Stages are simply a collection of ".yml" tasks files and directories located inside of the `engine` role in `tasks/`. These task files reference one or more functions within `tasks/funcs/<stage_name>/` and the entrypoint to every stage is `tasks/funcs/<stage_name>/main.yml`. This allows for the grouping of functions by type/use. Stages are called by defining the `me_stage` variable when calling a role:


```
- include_role: engine
  vars:
    me_stage: example_stage
    me_funcs:
      - function_one
      - function_two
      - etc
```

This example calls the tasks file located in `tasks/funcs/example_stage/main.yml`. That stage file then calls the three listed functions, in the order specified.

### Functions

Ulitimately, every set of a role's functionality can be divided into logical code blocks that roughly equivalent to a function in a typical programming language.

This allows the engine to be best used like an object of the role class with various methods (aka functions). In addition to the stage yaml files and the functions within `roles/engine__<version>/tasks/funcs/<stage_name>/`, there are several other task files that live within `roles/engine__<version>/tasks/libs/`. These task files are all called by top-level functions that live in the stage-specific directories. These should never be called directly since they are almost always dependent on inputs passed to them from one of the top-level functions. The usage of each stage and function is described in a different section of the framework's docs.

Here is an example of what the stages, functions, and libraries may look like.

```
roles/engine/tasks/
├── funcs
│   ├── prepare
│   │   ├── call_entry_role.yml
│   │   ├── get_role_deps.yml
│   │   └── main.yml
│   └── main.yml
├── libs
│   ├── install_role_deps.yml
│   ├── parse_role_deps.yml
│   └── parse_roles_present.yml
└── main.yml

```

In this example, there is one stage: prepare. That stage includes a `call_entry_role` and `get_role_deps` function. These functions may use one or more of the common libraries in the `libs` sub-directory to perform their work. Since these libraries are never called directly by end users, how and if they are called should be largely irrelevant to end users.

# Framework Usage by Engine Role Stage and Function

## Prepare Stage

### Purpose

The `prepare` stage is responsible for calling the functions that parse the entry role defined by the user, fully resolving and downloading all the dependencies of that role, and then calling the entry role.

### Usage

The prepare stage is designed to be the execution entrypoint of the maniac `engine` that is called by the playbook. As a result, the prepare stage is almost exclusively invoked by the main `playbook.yml`, as opposed to directly by the user. When invoked by maniac's `playbook.yml`, it is usually done so as follows:

```
git clone <insert_maniac_repo_url_here> maniac;
cd maniac;
ansible-playbook -i </insert/path/to/inventory_file_here> playbook.yml;
```

### Functions

#### get_role_deps

The `get_role_deps` function only requires one dictionary, `me_entry_role`, to be defined in `group_vars`. This dictionary should be placed in the `group_vars` for the group being called in the inventory file. It should contain the name of the desired entry role, it's URL, the version, and the entrypoint. It may optionally contain the sub-directory where the role is located and entrypoint.

```
me_entry_role:
  dest: "your_role_name_here" # mandatory, short name for the role
  repo: "url_for_your_git_repo_here" # mandatory, may be a local or remote git repo
  version: "branch_tag_or_commit_here" # optional, default is "master"
  subdir: "subdirectory_where_role_resides" # optional, default is none
  tasks_from: "name_of_file_in_tasks" # optional, default is "main"
```

The role dependency managmeent system is normally invoked via the main playbook.yml within the repository, but may also be called in isolation. This function will check for a file called `vars/me_role_deps.yml` in the entry role, download the dependencies listed there, and then continue reading this same file in each new dependency until there are no new roles to download. As a result, role authors wishing to use this interface will have to have created a file in their role with contents similar to this:

```
me_role_deps:
  - dest: "your_role_name_here" # mandatory, short name for the role
    repo: "url_for_your_git_repo_here" # mandatory, may be a local or remote git repo
    version: "branch_tag_or_commit_here" # optional, default is "master"
    subdir: "subdirectory_where_role_resides" # optional, default is none
  - dest: "your_other_role_name_here" # mandatory, short name for the role
    repo: "url_for_your_other_git_repo_here" # mandatory, may be a local or remote git repo
    version: "another_branch_tag_or_commit_here" # optional, default is "master"
    subdir: "subdirectory_where_role_resides" # optional, default is none
```

This function should almost always be execute via the main playbook.yml so that it downloads all the entry role's dependencies prior to executing any roles, but in the event that you have the need to use in function in isolation, use this template to call the function from another role:


```
- name: include maniac engine's prepare function
  include_role:
    name: "engine__<insert_version_here>"
    vars:
      me_stage: "prepare"
      me_funcs:
        - get_role_deps
      me_entry_role:
        dest: "<insert_role_name_here>"
        repo: "<insert_local_or_remote_git_repo_url_here>"
        version: "<insert_tag_branch_or_commit_here>"
```

This function is currently unit tested by invoking the `get_role_deps` scenario.

Testing for this function can be invoked as follows:

```
pip3 install molecule;
cd roles/engine/;
molecule test -s playbook;
```

The `get_role_deps` test scenario located in the `molecule` directory of the `engine` role executes the maniac `engine` role via the `playbook.yml` file located in the scenario's directory. Example vars and inventories can be found in the `molecule/get_role_deps/` directory. The `get_role_deps` scenario demonstrates how a role developer can assign an 'entry role' by placing a `me_entry_role` dictionary in a vars file, to tell the maniac framework what role the user intends to apply to a specific set of infrastructure. This dictionary should be placed in the `group_vars/` file associated wit the group referenced in the inventory file called. The entry role is downloaded and installed by the framework's `engine` role, into the `roles` directory with a `__<version>` suffix. This version number of the role, along with it's location are specified via the `me_entry_role` dictionary. The version number parameter uses the ansible git module on the backend, and therefore accepts either a git tag, branch name, or commit hash as a valid value. Once the entry role is downloaded and installed, the `engine` checks inside the role's `vars/` directory for a file called `me_role_deps.yml` containing a `me_role_deps` list in a similar format to the `me_entry_role` dictionary. This list should contain the location of the git repo containing the role, the name of the destination directory inside of the `roles/` directory, the tag, branch, or commit that will comprise the `__version` suffix appended to the destination directory name, and the subdir (if applicable) inside of the git repo that is the entrypoint of the ansible role. This method for specifying a role's dependencies can be repeated for each of the entry role's dependencies, their dependencies, etc. The engine will continue downloading the required version of each role's dependencies until it has them all installed, before proceeding to the execution of the entry role. This is demonstrated in the test scenario provided in the `molecule/` sub-directory. The directory `molecule/.mock` contains a set of mock git repos used to perform testing for this function. Once the dependency download is complete, `goss` is used by molecule to ensure the dependent roles exists in the roles directory.

#### call_entry_role

The call_entry_role function only requires one dictionary, `me_entry_role`. This dictionary should be placed in the `group_vars` for the group being called in the inventory file. It should contain the name of role, it's URL, the version, and the entrypoint. It may optionally contain the subdirectory where the role is located and entrypoint.


```
me_entry_role:
  dest: "your_role_name_here" # mandatory, short name for the role
  repo: "url_for_your_git_repo_here" # mandatory, may be a local or remote git repo
  version: "branch_tag_or_commit_here" # optional, default is "master"
  subdir: "subdirectory_where_role_resides" # optional, default is none
  tasks_from: "name_of_file_in_tasks" # optional, default is "main"
```

This entry role will be placed on the localhost calling ansible. It will be placed in the same directory that the engine role calling it resides in. The name of the role will be in the following format: `<role_name>__<version>`. Where `<role_name>` is the value provided to the `me_entry_role.dest` and `<version>` is the value provided to `me_entry_role.version`. Should the role reside in a subdirectory within the repository downloaded from the value of `me_entry_role.repo`, the role directory passed to `me_entry_role.subdir` only that subdirectory will be placed in the `roles/` directory on the localhost using the same naming schema. If the entry role does not use `main.yml` as it's entrypoint, and another tasks file within the role's `tasks/` directory should be the entrypoint, the name of the file (minus the ".yml" or ".yaml" extension) should be used as the value of `me_entry_role.tasks_from`. This will bypass `main.yml` and begin role execution in an alternate file.

This function is currently only tested by invoking the `playbook` scenario. Testing the function in isolation is planned future improvement.

Testing for this function can be invoked as follows:

```
pip3 install molecule;
cd roles/engine/;
molecule test -s playbook;
```

The `call_entry_role` test scenario located in the `molecule` directory of the `engine` role executes the maniac `engine` role via the `playbook.yml` file located in the scenario's directory. Example vars and inventories can be found in the `molecule/get_role_deps/` directory. The `call_entry_role` scenario demonstrates how a role developer can assign an 'entry role' by placing a `me_entry_role` dictionary in a vars file, to tell the maniac framework what role the user intends to apply to a specific set of infrastructure. This dictionary should be placed in the `group_vars/` file associated wit the group referenced in the inventory file called. The entry role is downloaded and installed by the framework's `engine` role, into the `roles` directory with a `__<version>` suffix. This version number of the role, along with it's location are specified via the `me_entry_role` dictionary. Once the entry role is downloaded and installed, the `engine` will call the entry role, which will result in file called "testfile" being created. The directory `molecule/.mock` contains the set of mock git repos used to perform testing for this function. Once the entry role is called, resulting in the creation of the test file, `goss` is used by molecule to ensure the file exists.

# Integration Testing 

Molecule is used for integration testing, in addition to unit testing of individual functions. 

The `playbook` test scenario located in the `molecule` directory of the `engine` role executes the maniac `engine` role via the `playbook.yml` file located in the top-level directory. Example vars and inventories can be found in the `molecule/playbook/` directory. The `playbook` scenario demonstrates how a role developer can assign an 'entry role' by placing a `me_entry_role` dictionary in a vars file, to tell the maniac framework what role the user intends to apply to a specific set of infrastructure. This dictionary should be placed in the `group_vars/` file associated wit the group referenced in the inventory file called. The entry role is downloaded and installed by the framework's `engine` role, into the `roles` directory with a `__<version>` suffix. This version number of the role, along with it's location are specified via the `me_entry_role` dictionary. The version number parameter uses the ansible git module on the backend, and therefore accepts either a git tag, branch name, or commit hash as a valid value. Once the entry role is downloaded and installed, the `engine` checks inside the role's `vars/` directory for a file called `me_role_deps.yml` containing a `me_role_deps` list in a similar format to the `me_entry_role` dictionary. This list should contain the location of the git repo containing the role, the name of the destination directory inside of the `roles/` directory, the tag, branch, or commit that will comprise the `__version` suffix appended to the destination directory name, and the subdir (if applicable) inside of the git repo that is the entrypoint of the ansible role. This method for specifying a role's dependencies can be repeated for each of the entry role's dependencies, their dependencies, etc. The engine will continue downloading the required version of each role's dependencies until it has them all installed, before proceeding to the execution of the entry role. This is demonstrated in the test scenario provided in the `molecule/` subdirectory. The directory `molecule/.mock` contains a set of mock git repos used to perform testing for this function. Once all dependency downloading is complete and the entry role has been called, checks are performed to ensure that both worked appropriately.

# License

MIT

# Author Information

Dreamer Labs
